#ifndef _OBSTACLE_H_
#define _OBSTACLE_H_

#include "jeu.h"
#include "construction.h"

#define NB_OBSTACLE 20

#define TRONC 0
#define TROU 1
#define PLOT 2
#define VOITURE 3
#define CAMION 4


typedef struct coordonnes{
    double x;
    double y;
    double z;
    int type;
    int couleur;
}coordonnes;

int generer_obstacles(int z_obstacle);

void afficher_obstacles();

int test_obstacle(int z_obstacle, double pos_x, double pos_y, double pos_z, double *sol);

#endif