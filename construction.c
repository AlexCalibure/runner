#include"construction.h"
#include "jeu.h"

void affiche_cube(double x1, double y1, double z1, double x2, double y2, double z2){
    glBegin(GL_QUADS);
    //Face Bas;
    glColor3ub(205, 125, 125);//Gris
    glVertex3f(x1, y1, z1);//A
    glVertex3f(x2, y1, z1);//D
    glVertex3f(x2, y1, z2);//E
    glVertex3f(x1, y1, z2);//F 

    //Face Avant
    glVertex3f(x1, y1, z1);//A
    glVertex3f(x2, y1, z1);//D
    glVertex3f(x2, y2, z1);//C
    glVertex3f(x1, y2, z1);//B

    //Face Gauche 
    glVertex3f(x1, y1, z1);//A
    glVertex3f(x1, y1, z2);//F
    glVertex3f(x1, y2, z2);//G
    glVertex3f(x1, y2, z1);//B
    
    //Face Arriere
    glVertex3f(x1, y1, z2);//F
    glVertex3f(x2, y1, z2);//E
    glVertex3f(x2, y2, z2);//H
    glVertex3f(x1, y2, z2);//G

    //Face Droite
    glVertex3f(x2, y1, z2);//E
    glVertex3f(x2, y1, z1);//D
    glVertex3f(x2, y2, z1);//C
    glVertex3f(x2, y2, z2);//H

    //Face Dessus
    glVertex3f(x1, y2, z1);//B
    glVertex3f(x2, y2, z1);//C
    glVertex3f(x2, y2, z2);//H
    glVertex3f(x1, y2, z2); //G

    glEnd();
}

void affiche_sphere(double x, double y, double z, double rayon, double r, double v, double b){
    glPushMatrix();
    glTranslated(x, y, z);
    glColor3ub(r, v, b);
    glutSolidSphere(rayon,100,100);
    glPopMatrix();
}

void affiche_armature(double x1, double y1, double z1, double x2, double y2, double z2){
    int i;

    glBegin(GL_QUADS);
    glColor3ub(200, 200, 200);//Gris
    glVertex3f(x1, y1, z1);//A
    glVertex3f(x2, y1, z1);//D
    glVertex3f(x2, y1, z2);//E
    glVertex3f(x1, y1, z2);//F
    
    glEnd();
    


    for(i=0; i<50; i++){
        
        glPushMatrix();
        glTranslatef(-0.5, 0.01, i*10);
    
        glBegin(GL_QUADS);
        glColor3ub(255, 255, 255);//Blanc
        glVertex3f(0, 0, 0);//A
        glVertex3f(0, 0, 2);//D
        glVertex3f(1, 0, 2);//E
        glVertex3f(1, 0, 0);//F
    
        glEnd();  
        glPopMatrix();
    }
     
}


void affiche_maison(double x1, double y1, double z1, int cote, int offset){
    double x2=x1+6, y2=y1+6, z2=z1+6;
       
    glPushMatrix();
    glTranslatef(LIMITE_X_MIN-x2-8, y1, offset);
    glRotatef(90, 0, 1, 0);
    
    glBegin(GL_QUADS);

    //bottom
    glColor3ub(209, 179, 115);
    glVertex3d(x1, y1, z1);
    glVertex3d(x2, y1, z1);
    glVertex3d(x2, y1, z2);
    glVertex3d(x1, y1, z2);

    //top
    glVertex3d(x1, y2, z1);
    glVertex3d(x2, y2, z1);
    glVertex3d(x2, y2, z2);
    glVertex3d(x1, y2, z2);

    //right
    glVertex3d(x1, y1, z1);
    glVertex3d(x1, y2, z1);
    glVertex3d(x1, y2, z2);
    glVertex3d(x1, y1, z2);

    //far
    glVertex3d(x1, y1, z2);
    glVertex3d(x1, y2, z2);
    glVertex3d(x2, y2, z2);
    glVertex3d(x2, y1, z2);

    //near
    glVertex3d(x1, y1, z1);
    glVertex3d(x1, y2, z1);
    glVertex3d(x2, y2, z1);
    glVertex3d(x2, y1, z1);


    //left
    glVertex3d(x2, y1, z1);
    glVertex3d(x2, y2, z1);
    glVertex3d(x2, y2, z2);
    glVertex3d(x2, y1, z2);

    //left rooftop
    glColor3ub(245, 115, 58);
    glVertex3d(x2, y2, z1-1);
    glVertex3d(x2, y2, z2+1);
    glVertex3d((x1+x2)/2, y2+(abs(y2)+abs(y1))/2, z2+1);
    glVertex3d((x1+x2)/2, y2+(abs(y2)+abs(y1))/2, z1-1);
    
    //right rooftop
    glVertex3d(x1, y2, z1-1);
    glVertex3d(x1, y2, z2+1);
    glVertex3d((x1+x2)/2, y2+(abs(y2)+abs(y1))/2, z2+1);
    glVertex3d((x1+x2)/2, y2+(abs(y2)+abs(y1))/2, z1-1);
    
    glEnd();

    //near rooftop
    glColor3ub(209, 179, 115);
    glBegin(GL_TRIANGLES);
    glVertex3d(x1, y2, z1);
    glVertex3d(x2, y2, z1);
    glVertex3d((x1+x2)/2, y2+(abs(y2)+abs(y1))/2, z1);
    glEnd();
    
    //near rooftop
    glBegin(GL_TRIANGLES);
    glVertex3d(x1, y2, z2);
    glVertex3d(x2, y2, z2);
    glVertex3d((x1+x2)/2, y2+(abs(y2)+abs(y1))/2, z2);    
    glEnd();

    //porte
    glBegin(GL_QUADS);
    glColor3ub(145, 85, 61);
    glVertex3d(2, 0, 6.1);
    glVertex3d(2, 3, 6.1);
    glVertex3d(3, 3, 6.1);
    glVertex3d(3, 0, 6.1);
    glEnd();

    //fenetre
    /*glBegin(GL_QUADS);
    glColor3ub(255, 255, 255);

    glVertex3d(-0.1, 1, 3);
    glVertex3d(-0.1, 2, 3);
    glVertex3d(-0.1, 2, 5);
    glVertex3d(-0.1, 1, 5);

    glEnd();*/
    
    glPopMatrix();
    
}

void affiche_arbre(double x, double y, double z){
    GLUquadric *param = gluNewQuadric();
    glColor3ub(150, 70, 30);
    
    glPushMatrix();
    glTranslated(x, y, z);
    glRotatef(90, -1, 0, 0);
    gluQuadricDrawStyle(param,GLU_FILL);
    gluCylinder(param, 1, 1, 5, 20, 1);
    glPopMatrix();
    
    affiche_sphere(x, y+5, z, 2.0, 0, 120, 0);
    
}

void affiche_poteau(double offset){

    double x1=0, y1=0, z1=0, x2=0.2, y2=5, z2=0.2;
    
    glPushMatrix();
    glTranslatef(LIMITE_X_MIN-4, 0, offset);
    glRotatef(90, 0, 1, 0);
    
    glBegin(GL_QUADS);

    //bottom
    glColor3f(0.7, 0.7, 0.7);
    glVertex3d(x1, y1, z1);
    glVertex3d(x2, y1, z1);
    glVertex3d(x2, y1, z2);
    glVertex3d(x1, y1, z2);

    //top
    glVertex3d(x1, y2, z1);
    glVertex3d(x2, y2, z1);
    glVertex3d(x2, y2, z2);
    glVertex3d(x1, y2, z2);

    //right
    glVertex3d(x1, y1, z1);
    glVertex3d(x1, y2, z1);
    glVertex3d(x1, y2, z2);
    glVertex3d(x1, y1, z2);

    //far
    glVertex3d(x1, y1, z2);
    glVertex3d(x1, y2, z2);
    glVertex3d(x2, y2, z2);
    glVertex3d(x2, y1, z2);

    //near
    glVertex3d(x1, y1, z1);
    glVertex3d(x1, y2, z1);
    glVertex3d(x2, y2, z1);
    glVertex3d(x2, y1, z1);


    //left
    glVertex3d(x2, y1, z1);
    glVertex3d(x2, y2, z1);
    glVertex3d(x2, y2, z2);
    glVertex3d(x2, y1, z2);
    
    
    glEnd();

    affiche_sphere(0, 4.8, 0, 0.5, 200, 200, 0);
    
    glPopMatrix();
    
}

void affiche_jardin(){        
    glBegin(GL_QUADS);
    glColor3ub(0, 180, 0);
    glVertex3f(-40, -0.01, -40);//A
    glVertex3f(-40, -0.01, 1000);//D
    glVertex3f(40, -0.01, 1000);//E
    glVertex3f(40, -0.01, -40);//F 
    glEnd();
}

void affiche_haie(){
    double x1=LIMITE_X_MAX+25, y1=0, z1=0, x2=LIMITE_X_MAX+20, y2=4, z2=LIMITE_Z_MAX;
       
    glBegin(GL_QUADS);
    glColor3ub(0, 100, 0);
    
    //Face Bas;
    glVertex3f(x1, y1, z1);//A
    glVertex3f(x2, y1, z1);//D
    glVertex3f(x2, y1, z2);//E
    glVertex3f(x1, y1, z2);//F 

    //Face Avant
    glVertex3f(x1, y1, z1);//A
    glVertex3f(x2, y1, z1);//D
    glVertex3f(x2, y2, z1);//C
    glVertex3f(x1, y2, z1);//B

    //Face Gauche
    glVertex3f(x1, y1, z1);//A
    glVertex3f(x1, y1, z2);//F
    glVertex3f(x1, y2, z2);//G
    glVertex3f(x1, y2, z1);//B
    
    //Face Arriere
    glVertex3f(x1, y1, z2);//F
    glVertex3f(x2, y1, z2);//E
    glVertex3f(x2, y2, z2);//H
    glVertex3f(x1, y2, z2);//G

    //Face Droite
    glVertex3f(x2, y1, z2);//E
    glVertex3f(x2, y1, z1);//D
    glVertex3f(x2, y2, z1);//C
    glVertex3f(x2, y2, z2);//H

    //Face Dessus
    glVertex3f(x1, y2, z1);//B
    glVertex3f(x2, y2, z1);//C
    glVertex3f(x2, y2, z2);//H
    glVertex3f(x1, y2, z2); //G
    
    glEnd();
}

void affiche_montagne(){
    double z=LIMITE_Z_MAX;

    glColor3ub(162, 142, 140);
    glBegin(GL_TRIANGLES);
    glVertex3d(LIMITE_X_MAX-50, 0, z);
    glVertex3d(LIMITE_X_MIN+50, 0, z);
    glVertex3d(0, 50, z);
    glEnd();


    glColor3ub(240, 240, 240);
    glBegin(GL_TRIANGLES);
    glVertex3d(LIMITE_X_MAX-15, 40, z-0.1);
    glVertex3d(LIMITE_X_MIN+15, 40, z-0.1);
    glVertex3d(0, 50, z-0.1);
    glEnd();

    glColor3ub(50, 50, 50);
    glPushMatrix();
    glTranslatef(0, 0, z-0.1);
    GLUquadric *param = gluNewQuadric();
    gluQuadricDrawStyle(param,GLU_FILL);
    gluDisk(param, 0, 5, 200, 1);
    glPopMatrix();
}

void affiche_decor(double z){
    int i;
    
    affiche_jardin();

    affiche_haie();

    glPushMatrix();
    glTranslatef(LIMITE_X_MIN-25-(LIMITE_X_MAX+25), 0, 0);
    affiche_haie();
    
    glPopMatrix();

    for(i=0; i<50; i=i+3){
        affiche_arbre(LIMITE_X_MIN-8, 0, i*10+10);
        affiche_maison(0, 0, 0, 1, i*10+20);
        affiche_poteau(i*10+25);
    }

      
    glPushMatrix();
    glTranslatef(0, 0, LIMITE_Z_MAX);
    glRotatef(180, 0, 1, 0);
    
    for(i=0; i<50; i=i+3){
        affiche_arbre(LIMITE_X_MIN-8, 0, i*10+10);
        affiche_maison(0, 0, 0, 1, i*10+20);
        affiche_poteau(i*10+25);
    }
    
    glPopMatrix();

    affiche_montagne();
    
}

void affiche_voiture(double x, double y, double z, int angle, int couleur){

    GLUquadric *param = gluNewQuadric();
    gluQuadricDrawStyle(param,GLU_FILL);
    //si on veut avoir la voiture de face ou de dos
    if(angle==1){
        glPushMatrix();
        glTranslatef(x, y, z);
        glRotatef(180, 0, 1, 0);
        x=2;
        z=-5;
    }

    glPushMatrix();
    glTranslatef(x, y+0.5, z);
    //glRotatef(90, 0, 1, 0);
    
    glBegin(GL_POLYGON);
   
    if(couleur==1){
        glColor3ub(100, 200, 0);
    }else if(couleur==2){
        glColor3ub(0, 100, 200);
    }else{
        glColor3ub(200, 0, 0);
    }

    glVertex3d(0, 0, 0);
    glVertex3d(0, 2, 0);
    glVertex3d(0, 2, 3);
    glVertex3d(0, 1.2, 4);
    glVertex3d(0, 1.2, 4.5);
    glVertex3d(0, 0.8, 5);
    glVertex3d(0, 0, 5);
    
    glEnd();
    
    //pneu
    glColor3ub(100, 100, 100);

    //avant gauche
    glPushMatrix();
    glTranslatef(0.01, 0, 3.8);
    glRotatef(90, 0, 1, 0);
    gluDisk(param, 0, 0.5, 20, 1);
    glPopMatrix();

    //arriere gauche
    glPushMatrix();
    glTranslatef(0.01, 0, 0.8);
    glRotatef(90, 0, 1, 0);
    gluDisk(param, 0, 0.5, 20, 1);
    glPopMatrix();


    //fenetre avant
    glBegin(GL_POLYGON);
   
    glColor3ub(0, 0, 0);

    glVertex3d(0.01, 1.2, 2);
    glVertex3d(0.01, 1.8, 2);
    glVertex3d(0.01, 1.8, 2.8);
    glVertex3d(0.01, 1.2, 3.55);
    
    glEnd();

    //fenetre arriere

    glBegin(GL_POLYGON);

    glVertex3d(0.01, 1.2, 0.4);
    glVertex3d(0.01, 1.8, 0.4);
    glVertex3d(0.01, 1.8, 1.6);
    glVertex3d(0.01, 1.2, 1.6);

    glEnd();
    
    glPopMatrix();



    /**********************************/
    //deuxieme face

    glPushMatrix();
    glTranslatef(x-2, y+0.5, z);
    //glRotatef(90, 0, 1, 0);
    
    glBegin(GL_POLYGON);
   
    if(couleur==1){
        glColor3ub(100, 200, 0);
    }else if(couleur==2){
        glColor3ub(0, 100, 200);
    }else{
        glColor3ub(200, 0, 0);
    }
    glVertex3d(0, 0, 0);
    glVertex3d(0, 2, 0);
    glVertex3d(0, 2, 3);
    glVertex3d(0, 1.2, 4);
    glVertex3d(0, 1.2, 4.5);
    glVertex3d(0, 0.8, 5);
    glVertex3d(0, 0, 5);
    
    glEnd();
    
    //pneu
    glColor3ub(100, 100, 100);

    //avant gauche
    glPushMatrix();
    glTranslatef(-0.01, 0, 3.8);
    glRotatef(90, 0, 1, 0);
    gluDisk(param, 0, 0.5, 20, 1);
    glPopMatrix();

    //arriere gauche
    glPushMatrix();
    glTranslatef(-0.01, 0, 0.8);
    glRotatef(90, 0, 1, 0);
    gluDisk(param, 0, 0.5, 20, 1);
    glPopMatrix();


    //fenetre avant
    glBegin(GL_POLYGON);
   
    glColor3ub(0, 0, 0);

    glVertex3d(-0.01, 1.2, 2);
    glVertex3d(-0.01, 1.8, 2);
    glVertex3d(-0.01, 1.8, 2.8);
    glVertex3d(-0.01, 1.2, 3.55);
    
    glEnd();

    //fenetre arriere
    glBegin(GL_POLYGON);

    glVertex3d(-0.01, 1.2, 0.4);
    glVertex3d(-0.01, 1.8, 0.4);
    glVertex3d(-0.01, 1.8, 1.6);
    glVertex3d(-0.01, 1.2, 1.6);

    glEnd();
    
    glPopMatrix();
    
    //toit  
    glPushMatrix();
    glTranslatef(x, y+0.5, z);

    if(couleur==1){
        glColor3ub(100, 200, 0);
    }else if(couleur==2){
        glColor3ub(0, 100, 200);
    }else{
        glColor3ub(200, 0, 0);
    }

    glBegin(GL_QUADS);

    glVertex3d(0, 0, 0);
    glVertex3d(0, 2, 0);
    glVertex3d(-2, 2, 0);
    glVertex3d(-2, 0, 0);

    glVertex3d(0, 2, 0);
    glVertex3d(0, 2, 3);
    glVertex3d(-2, 2, 3);
    glVertex3d(-2, 2, 0);

    glVertex3d(0, 2, 3);
    glVertex3d(0, 1.2, 4);
    glVertex3d(-2, 1.2, 4);
    glVertex3d(-2, 2, 3);

    glVertex3d(0, 1.2, 4.5);
    glVertex3d(0, 1.2, 4);    
    glVertex3d(-2, 1.2, 4);
    glVertex3d(-2, 1.2, 4.5);

    glVertex3d(0, 1.2, 4.5);
    glVertex3d(0, 0.8, 5);
    glVertex3d(-2, 0.8, 5);
    glVertex3d(-2, 1.2, 4.5);

    
    glVertex3d(0, 0, 5);
    glVertex3d(0, 0.8, 5);
    glVertex3d(-2, 0.8, 5);
    glVertex3d(-2, 0, 5);
    

    glColor3ub(0, 0, 0);
    glVertex3d(-0.2, 1.955555, 3.01);
    glVertex3d(-0.2, 1.3, 4.01);
    glVertex3d(-1.8, 1.3, 4.01);
    glVertex3d(-1.8, 1.955555, 3.01);


    glColor3ub(200, 200, 0);
    glVertex3d(-0.1, 1.2, 4.55);
    glVertex3d(-0.1, 0.8, 5.05);
    glVertex3d(-0.6, 0.8, 5.05);
    glVertex3d(-0.6, 1.2, 4.55);

    glVertex3d(-1.4, 1.2, 4.55);
    glVertex3d(-1.4, 0.8, 5.05);
    glVertex3d(-1.9, 0.8, 5.05);
    glVertex3d(-1.9, 1.2, 4.55);


    //coffre

    //pahre arrieres
    glColor3ub(200, 60, 60);
    glVertex3d(-0.1, 0.1, -0.01);
    glVertex3d(-0.1, 0.4, -0.01);
    glVertex3d(-0.4, 0.4, -0.01);
    glVertex3d(-0.4, 0.1, -0.01);

    glVertex3d(-1.6, 0.1, -0.01);
    glVertex3d(-1.6, 0.4, -0.01);
    glVertex3d(-1.9, 0.4, -0.01);
    glVertex3d(-1.9, 0.1, -0.01);

    glVertex3d(-0.7, 1.8, -0.02);
    glVertex3d(-0.7, 1.9, -0.02);
    glVertex3d(-1.3, 1.9, -0.02);
    glVertex3d(-1.3, 1.8, -0.02);

    //fenetre arriere coffre
    glColor3ub(0, 0, 0);
    glVertex3d(-0.2, 1.2, -0.01);
    glVertex3d(-0.2, 1.8, -0.01);
    glVertex3d(-1.8, 1.8, -0.01);
    glVertex3d(-1.8, 1.2, -0.01);

    glEnd();
    glPopMatrix();

    if(angle==1){
        glPopMatrix();
    }

}


void affiche_plot(double x, double y, double z){
    GLUquadric *param = gluNewQuadric();
    gluQuadricDrawStyle(param,GLU_FILL);

    glPushMatrix();
    glTranslatef(x, y, z);
    glRotatef(90, -1, 0, 0);

    glColor3ub(236, 105, 25);
    gluCylinder(param, 0.5, 0.1, 1.2, 20, 1);

    glPushMatrix();
    glTranslatef(0, 0, 0.201);

    glColor3ub(255, 255, 255);
    gluCylinder(param, 0.4333, 0.36666, 0.2, 20, 1);
    
    glPopMatrix();

    glPushMatrix();
    glTranslatef(0, 0, 0.601);

    glColor3ub(255, 255, 255);
    gluCylinder(param, 0.3, 0.23333, 0.2, 20, 1);
    
    glPopMatrix();

    glPopMatrix();

    glPushMatrix();
    glTranslatef(x, y, z);
    glBegin(GL_QUADS); 

    glColor3ub(236, 105, 25);
    glVertex3d(-0.6, 0.02, -0.6);
    glVertex3d(-0.6, 0.02, 0.6);
    glVertex3d(0.6, 0.02, 0.6);
    glVertex3d(0.6, 0.02, -0.6);

    glEnd();
    glPopMatrix();
}

void affiche_trou(double x, double y, double z){
    GLUquadric *param = gluNewQuadric();
    gluQuadricDrawStyle(param,GLU_FILL);

    glPushMatrix();
    glTranslatef(x, y+0.02, z);
    glRotatef(90, -1, 0, 0);
    glColor3ub(20, 20, 20);
    gluDisk(param, 0, 2, 20, 1);
    glPopMatrix();

}

void affiche_tronc(double z ){
    glPushMatrix();
    glTranslatef(LIMITE_X_MIN,0,z);
    glRotatef(90,0,1,0);
    glColor3ub(88, 41, 7);

    GLUquadric *param = gluNewQuadric();
    gluQuadricDrawStyle(param,GLU_FILL);
    gluCylinder(param,1,1,LIMITE_X_MAX - LIMITE_X_MIN,100,1);
    glPopMatrix(); 
}

void affiche_camion(double x, double y, double z, int angle){

    GLUquadric *param = gluNewQuadric();
    gluQuadricDrawStyle(param,GLU_FILL);

    //si on veut avoir le camion de face ou de dos
    if(angle==1){
        glPushMatrix();
        glTranslatef(x, y, z);
        glRotatef(180, 0, 1, 0);
        x=4;
        z=-10;
    }

    glPushMatrix();
    glTranslatef(x, y+1, z);
    //glRotatef(90, 0, 1, 0);
    
    //profil
    glBegin(GL_POLYGON);
   
    glColor3ub(200, 0, 0);

    glVertex3d(0, 0, 0);
    glVertex3d(0, 4, 0);
    glVertex3d(0, 4, 9);
    glVertex3d(0, 2.5, 10);
    glVertex3d(0, 0, 10);
    
    glEnd();
    
    //pneu
    glColor3ub(100, 100, 100);

    //avant gauche
    glPushMatrix();
    glTranslatef(0.06, 0, 8.3);
    glRotatef(90, 0, 1, 0);
    gluDisk(param, 0, 1, 20, 1);
    glPopMatrix();

    //arriere gauche
    glPushMatrix();
    glTranslatef(0.06, 0, 1.8);
    glRotatef(90, 0, 1, 0);
    gluDisk(param, 0, 1, 20, 1);
    glPopMatrix();


    //fenetre avant
    glBegin(GL_POLYGON);
   
    glColor3ub(0, 0, 0);

    glVertex3d(0.07, 3.8, 7.5);
    glVertex3d(0.07, 3.8, 8.8);
    glVertex3d(0.07, 2.5, 9.6666);
    glVertex3d(0.07, 1.7, 9.6666);
    glVertex3d(0.07, 1.7, 8.5);
    glVertex3d(0.07, 2, 8.5);
    glVertex3d(0.07, 2, 7.5);
    
    glEnd();

    //poignee
    glPushMatrix();
    glTranslatef(0.07, 1.8, 8.2);
    glRotatef(90, 0, 1, 0);
    glColor3ub(50, 50, 50);
    gluDisk(param, 0, 0.1, 20, 1);
    glPopMatrix();


    //protection

    glColor3ub(150, 150, 150);

    glBegin(GL_POLYGON);
   
    glVertex3d(0.05, 0.2, 0);
    glVertex3d(0.05, 0.3, 0);
    glVertex3d(0.05, 0.3, 10);
    glVertex3d(0.05, 0.2, 10);
    
    glEnd();

    glBegin(GL_POLYGON);
   
    glVertex3d(0.05, 0.5, 0);
    glVertex3d(0.05, 0.6, 0);
    glVertex3d(0.05, 0.6, 10);
    glVertex3d(0.05, 0.5, 10);
    
    glEnd();


    glColor3ub(60, 60, 60);
    glBegin(GL_POLYGON);
   
    glVertex3d(0.01, 0, 0);
    glVertex3d(0.01, 1.1, 0);
    glVertex3d(0.01, 1.1, 10);
    glVertex3d(0.01, 0, 10);
    
    glEnd();

    //separation
    glColor3ub(0, 0, 0);
    glBegin(GL_LINES);
    glVertex3d(0.01, 1.1, 7);
    glVertex3d(0.01, 4, 7);
    glEnd();

    //ecriture
    glColor3ub(255, 255, 255);

    glBegin(GL_POLYGON);
    glVertex3d(0.01, 3.2, 2);
    glVertex3d(0.01, 3.5, 2);
    glVertex3d(0.01, 3.5, 5);
    glVertex3d(0.01, 3.2, 5);
    glEnd();

    glBegin(GL_POLYGON);
    glVertex3d(0.01, 2.2, 2);
    glVertex3d(0.01, 2.5, 2);
    glVertex3d(0.01, 2.5, 5);
    glVertex3d(0.01, 2.2, 5);
    glEnd();

    glPopMatrix();

 /**********************************/
    //deuxieme face
    glPushMatrix();
    glTranslatef(x-4, y+1, z);
    //glRotatef(-90, 0, 1, 0);
    
    //profil
    glBegin(GL_POLYGON);
   
    glColor3ub(200, 0, 0);

    glVertex3d(0, 0, 0);
    glVertex3d(0, 4, 0);
    glVertex3d(0, 4, 9);
    glVertex3d(0, 2.5, 10);
    glVertex3d(0, 0, 10);
    
    glEnd();
    
    //pneu
    glColor3ub(100, 100, 100);

    //avant droit
    glPushMatrix();
    glTranslatef(-0.06, 0, 8.3);
    glRotatef(90, 0, 1, 0);
    gluDisk(param, 0, 1, 20, 1);
    glPopMatrix();

    //arriere droit
    glPushMatrix();
    glTranslatef(-0.06, 0, 1.8);
    glRotatef(90, 0, 1, 0);
    gluDisk(param, 0, 1, 20, 1);
    glPopMatrix();


    //fenetre avant
    glBegin(GL_POLYGON);
   
    glColor3ub(0, 0, 0);

    glVertex3d(-0.07, 3.8, 7.5);
    glVertex3d(-0.07, 3.8, 8.8);
    glVertex3d(-0.07, 2.5, 9.6666);
    glVertex3d(-0.07, 1.7, 9.6666);
    glVertex3d(-0.07, 1.7, 8.5);
    glVertex3d(-0.07, 2, 8.5);
    glVertex3d(-0.07, 2, 7.5);
    
    glEnd();

    //poignee
    glPushMatrix();
    glTranslatef(-0.07, 1.8, 8.2);
    glRotatef(90, 0, 1, 0);
    glColor3ub(50, 50, 50);
    gluDisk(param, 0, 0.1, 20, 1);
    glPopMatrix();


    //protection

    glColor3ub(150, 150, 150);

    glBegin(GL_POLYGON);
   
    glVertex3d(-0.05, 0.2, 0);
    glVertex3d(-0.05, 0.3, 0);
    glVertex3d(-0.05, 0.3, 10);
    glVertex3d(-0.05, 0.2, 10);
    
    glEnd();

    glBegin(GL_POLYGON);
   
    glVertex3d(-0.05, 0.5, 0);
    glVertex3d(-0.05, 0.6, 0);
    glVertex3d(-0.05, 0.6, 10);
    glVertex3d(-0.05, 0.5, 10);
    
    glEnd();


    glColor3ub(60, 60, 60);
    glBegin(GL_POLYGON);
   
    glVertex3d(-0.01, 0, 0);
    glVertex3d(-0.01, 1.1, 0);
    glVertex3d(-0.01, 1.1, 10);
    glVertex3d(-0.01, 0, 10);
    
    glEnd();

    //separation
    glColor3ub(0, 0, 0);
    glBegin(GL_LINES);
    glVertex3d(-0.01, 1.1, 7);
    glVertex3d(-0.01, 4, 7);
    glEnd();

    //ecriture
    glColor3ub(255, 255, 255);

    glBegin(GL_POLYGON);
    glVertex3d(-0.02, 3.2, 1);
    glVertex3d(-0.02, 3.5, 1);
    glVertex3d(-0.02, 3.5, 3);
    glVertex3d(-0.02, 3.2, 3);
    glEnd();

    glBegin(GL_POLYGON);
    glVertex3d(-0.02, 2.3, 1.7);
    glVertex3d(-0.02, 2.7, 1.7);
    glVertex3d(-0.02, 2.7, 2.3);
    glVertex3d(-0.02, 2.3, 2.3);
    glEnd();

    glBegin(GL_POLYGON);
    glVertex3d(-0.02, 1.6, 1);
    glVertex3d(-0.02, 1.9, 1);
    glVertex3d(-0.02, 1.9, 3);
    glVertex3d(-0.02, 1.6, 3);
    glEnd();

    //cadre jaune
    glColor3ub(255, 200, 0);
    glBegin(GL_POLYGON);
    glVertex3d(-0.005, 1.3, 0.2);
    glVertex3d(-0.005, 3.8, 0.2);
    glVertex3d(-0.005, 3.8, 3.8);
    glVertex3d(-0.005, 1.3, 3.8);
    glEnd();

    glColor3ub(200, 0, 0);
    glBegin(GL_POLYGON);
    glVertex3d(-0.01, 1.5, 0.4);
    glVertex3d(-0.01, 3.6, 0.4);
    glVertex3d(-0.01, 3.6, 3.6);
    glVertex3d(-0.01, 1.5, 3.6);
    glEnd();

    //coffre cote
    glColor3ub(150, 150, 150);
    glBegin(GL_POLYGON);
    glVertex3d(-0.01, 1.5, 4.4);
    glVertex3d(-0.01, 3.6, 4.4);
    glVertex3d(-0.01, 3.6, 6.6);
    glVertex3d(-0.01, 1.5, 6.6);
    glEnd();

    glColor3ub(0, 0, 0);
    glBegin(GL_LINES);

    glVertex3d(-0.02, 1.6, 5.3);
    glVertex3d(-0.02, 1.6, 5.7);
    
    glVertex3d(-0.02, 1.7, 4.4);
    glVertex3d(-0.02, 1.7, 6.6);
    
    glVertex3d(-0.02, 1.9, 4.4);
    glVertex3d(-0.02, 1.9, 6.6);

    glVertex3d(-0.02, 2.1, 4.4);
    glVertex3d(-0.02, 2.1, 6.6);

    glVertex3d(-0.02, 2.3, 4.4);
    glVertex3d(-0.02, 2.3, 6.6);

    glVertex3d(-0.02, 2.5, 4.4);
    glVertex3d(-0.02, 2.5, 6.6);

    glVertex3d(-0.02, 2.7, 4.4);
    glVertex3d(-0.02, 2.7, 6.6);

    glVertex3d(-0.02, 2.9, 4.4);
    glVertex3d(-0.02, 2.9, 6.6);

    glVertex3d(-0.02, 3.1, 4.4);
    glVertex3d(-0.02, 3.1, 6.6);

    glVertex3d(-0.02, 3.3, 4.4);
    glVertex3d(-0.02, 3.3, 6.6);

    glVertex3d(-0.02, 3.5, 4.4);
    glVertex3d(-0.02, 3.5, 6.6);

    glEnd();

    glPopMatrix();

    glPushMatrix();
    glTranslatef(x, y+1, z);

    /*armature*/  
    glBegin(GL_QUADS);

    //coffre
    glColor3ub(200, 0, 0);
    glVertex3d(0, 0, 0);
    glVertex3d(0, 4, 0);
    glVertex3d(-4, 4, 0);
    glVertex3d(-4, 0, 0);

    glColor3ub(60, 60, 60);
    glVertex3d(0, 0, -0.001);
    glVertex3d(0, 1.1, -0.001);
    glVertex3d(-4, 1.1, -0.001);
    glVertex3d(-4, 0, -0.001);

    //phares arriere
    glColor3ub(200, 60, 60);
    glVertex3d(-0.1, 0.1, -0.005);
    glVertex3d(-0.1, 0.6, -0.005);
    glVertex3d(-0.6, 0.6, -0.005);
    glVertex3d(-0.6, 0.1, -0.005);

    glVertex3d(-3.4, 0.1, -0.005);
    glVertex3d(-3.4, 0.6, -0.005);
    glVertex3d(-3.9, 0.6, -0.005);
    glVertex3d(-3.9, 0.1, -0.005);

    glEnd();

    //porte
    glColor3ub(0, 0, 0);
    glBegin(GL_LINES);
    glVertex3d(-2, 1.1, -0.005);
    glVertex3d(-2, 3.9, -0.005);
    glEnd();
    
    glBegin(GL_QUADS);

    //coffre
    glColor3ub(200, 200, 0);
    glVertex3d(-0.4, 1.3, -0.005);
    glVertex3d(-0.4, 1.5, -0.005);
    glVertex3d(-1.8, 1.5, -0.005);
    glVertex3d(-1.8, 1.3, -0.005);

    glVertex3d(-0.4, 1.7, -0.005);
    glVertex3d(-0.4, 1.9, -0.005);
    glVertex3d(-1.8, 1.9, -0.005);
    glVertex3d(-1.8, 1.7, -0.005);

    glVertex3d(-0.4, 2.1, -0.005);
    glVertex3d(-0.4, 2.3, -0.005);
    glVertex3d(-1.4, 2.3, -0.005);
    glVertex3d(-1.4, 2.1, -0.005);

    glVertex3d(-0.4, 2.5, -0.005);
    glVertex3d(-0.4, 2.7, -0.005);
    glVertex3d(-1.8, 2.7, -0.005);
    glVertex3d(-1.8, 2.5, -0.005);

    glVertex3d(-0.4, 2.9, -0.005);
    glVertex3d(-0.4, 3.1, -0.005);
    glVertex3d(-1.8, 3.1, -0.005);
    glVertex3d(-1.8, 2.9, -0.005);

    glVertex3d(-0.4, 3.3, -0.005);
    glVertex3d(-0.4, 3.5, -0.005);
    glVertex3d(-1.8, 3.5, -0.005);
    glVertex3d(-1.8, 3.3, -0.005);

    glVertex3d(-2.2, 1.3, -0.005);
    glVertex3d(-2.2, 1.5, -0.005);
    glVertex3d(-3.6, 1.5, -0.005);
    glVertex3d(-3.6, 1.3, -0.005);

    glVertex3d(-2.2, 1.7, -0.005);
    glVertex3d(-2.2, 1.9, -0.005);
    glVertex3d(-3.6, 1.9, -0.005);
    glVertex3d(-3.6, 1.7, -0.005);

    glVertex3d(-2.6, 2.1, -0.005);
    glVertex3d(-2.6, 2.3, -0.005);
    glVertex3d(-3.6, 2.3, -0.005);
    glVertex3d(-3.6, 2.1, -0.005);

    glVertex3d(-2.2, 2.5, -0.005);
    glVertex3d(-2.2, 2.7, -0.005);
    glVertex3d(-3.6, 2.7, -0.005);
    glVertex3d(-3.6, 2.5, -0.005);

    glVertex3d(-2.2, 2.9, -0.005);
    glVertex3d(-2.2, 3.1, -0.005);
    glVertex3d(-3.6, 3.1, -0.005);
    glVertex3d(-3.6, 2.9, -0.005);

    glVertex3d(-2.2, 3.3, -0.005);
    glVertex3d(-2.2, 3.5, -0.005);
    glVertex3d(-3.6, 3.5, -0.005);
    glVertex3d(-3.6, 3.3, -0.005);

    //poignee
    glColor3ub(0, 0, 0);

    glVertex3d(-1.6, 2.1, -0.005);
    glVertex3d(-1.6, 2.3, -0.005);
    glVertex3d(-1.8, 2.3, -0.005);
    glVertex3d(-1.8, 2.1, -0.005);

    glVertex3d(-2.2, 2.1, -0.005);
    glVertex3d(-2.2, 2.3, -0.005);
    glVertex3d(-2.4, 2.3, -0.005);
    glVertex3d(-2.4, 2.1, -0.005);

    //toit
    glColor3ub(200, 0, 0);
    glVertex3d(0, 4, 0);
    glVertex3d(0, 4, 9);
    glVertex3d(-4, 4, 9);
    glVertex3d(-4, 4, 0);

    //vitre avant
    glColor3ub(200, 0, 0);
    glVertex3d(0, 2.5, 10);
    glVertex3d(0, 4, 9);
    glVertex3d(-4, 4, 9);
    glVertex3d(-4, 2.5, 10);

    glColor3ub(0, 0, 0);
    glVertex3d(-0.3, 2.6, 10);
    glVertex3d(-0.3, 3.8, 9.2);
    glVertex3d(-3.7, 3.8, 9.2);
    glVertex3d(-3.7, 2.6, 10);

    //avant
    glColor3ub(200, 0, 0);
    glVertex3d(0, 0, 10);
    glVertex3d(0, 2.5, 10);
    glVertex3d(-4, 2.5, 10);
    glVertex3d(-4, 0, 10);

    glColor3ub(240, 240, 240);
    glVertex3d(0, 0, 10.001);
    glVertex3d(0, 1.1, 10.001);
    glVertex3d(-4, 1.1, 10.001);
    glVertex3d(-4, 0, 10.001);

    //phares avant
    glColor3ub(200, 200, 0);
    glVertex3d(-0.1, 0.5, 10.005);
    glVertex3d(-0.1, 1, 10.005);
    glVertex3d(-0.6, 1, 10.005);
    glVertex3d(-0.6, 0.5, 10.005);

    glVertex3d(-3.4, 0.5, 10.005);
    glVertex3d(-3.4, 1, 10.005);
    glVertex3d(-3.9, 1, 10.005);
    glVertex3d(-3.9, 0.5, 10.005);

    //bande jaune 
    glVertex3d(-0.5, 2, 10.005);
    glVertex3d(-0.5, 2.3, 10.005);
    glVertex3d(-3.5, 2.3, 10.005);
    glVertex3d(-3.5, 2, 10.005);

    //marque  
    glColor3ub(150, 150, 150);  
    glVertex3d(-2, 1.3, 10.005);
    glVertex3d(-1.8, 1.5, 10.005);
    glVertex3d(-2, 1.7, 10.005);
    glVertex3d(-2.2, 1.5, 10.005);

    glEnd();


    //separation
    glColor3ub(0, 0, 0);
    glBegin(GL_LINES);
    glVertex3d(0, 4.01, 7);
    glVertex3d(-4, 4.01, 7);
    glEnd();

    glPopMatrix();  


    if(angle==1){
        glPopMatrix();
    }

}

void affiche_personnage(double x, double y, double z, double angle){
    glPushMatrix();
    glTranslatef(x, y, z);
    //tete
    affiche_sphere(0, 0, 0, .5, 255, 218, 187);
    //buste
    affiche_cube(-0.3, -1, -0.3, 0.3, -0.2, 0.3);

    //bras gauche
    glPushMatrix();
    glTranslatef(0.3, -0.5, 0.1);
    glRotatef(angle, 0.5, 0, 0);

    glColor3ub(255, 218, 187);
    glBegin(GL_QUADS);

    glVertex3d(0, 0, 0);
    glVertex3d(0, 0.5, 0);
    glVertex3d(0.2, 0.5, 0);
    glVertex3d(0.2, 0, 0);

    glVertex3d(0, 0, 0);
    glVertex3d(0, 0.5, 0);
    glVertex3d(0, 0.5, 0.2);
    glVertex3d(0, 0, 0.2);

    glVertex3d(0, 0, 0.2);
    glVertex3d(0, 0.5, 0.2);
    glVertex3d(0.2, 0.5, 0.2);
    glVertex3d(0.2, 0, 0.2);

    glVertex3d(0.2, 0, 0);
    glVertex3d(0.2, 0.5, 0);
    glVertex3d(0.2, 0.5, 0.2);
    glVertex3d(0.2, 0, 0.2);

    glVertex3d(0, 0.5, 0);
    glVertex3d(0.2, 0.5, 0);
    glVertex3d(0.2, 0.5, 0.2);
    glVertex3d(0, 0.5, 0.2);

    glVertex3d(0, 0, 0);
    glVertex3d(0.2, 0, 0);
    glVertex3d(0.2, 0, 0.2);
    glVertex3d(0, 0, 0.2);

    glEnd();

    glPopMatrix();

    //bras droit
    glPushMatrix();
    glTranslatef(-0.5, -0.5, 0.1);
    glRotatef(-angle, 0.5, 0, 0);

    glBegin(GL_QUADS);

    glVertex3d(0, 0, 0);
    glVertex3d(0, 0.5, 0);
    glVertex3d(0.2, 0.5, 0);
    glVertex3d(0.2, 0, 0);

    glVertex3d(0, 0, 0);
    glVertex3d(0, 0.5, 0);
    glVertex3d(0, 0.5, 0.2);
    glVertex3d(0, 0, 0.2);

    glVertex3d(0, 0, 0.2);
    glVertex3d(0, 0.5, 0.2);
    glVertex3d(0.2, 0.5, 0.2);
    glVertex3d(0.2, 0, 0.2);

    glVertex3d(0.2, 0, 0);
    glVertex3d(0.2, 0.5, 0);
    glVertex3d(0.2, 0.5, 0.2);
    glVertex3d(0.2, 0, 0.2);

    glVertex3d(0, 0.5, 0);
    glVertex3d(0.2, 0.5, 0);
    glVertex3d(0.2, 0.5, 0.2);
    glVertex3d(0, 0.5, 0.2);

    glVertex3d(0, 0, 0);
    glVertex3d(0.2, 0, 0);
    glVertex3d(0.2, 0, 0.2);
    glVertex3d(0, 0, 0.2);

    glEnd();

    glPopMatrix();


    //jambe droite
    glPushMatrix();
    glTranslatef(-0.3, -1, 0.1);
    glRotatef(angle, 0.5, 0, 0);

    glColor3ub(57, 63, 81);
    glBegin(GL_QUADS);

    glVertex3d(0, 0, 0);
    glVertex3d(0, 1, 0);
    glVertex3d(0.2, 1, 0);
    glVertex3d(0.2, 0, 0);

    glVertex3d(0, 0, 0);
    glVertex3d(0, 1, 0);
    glVertex3d(0, 1, 0.2);
    glVertex3d(0, 0, 0.2);

    glVertex3d(0, 0, 0.2);
    glVertex3d(0, 1, 0.2);
    glVertex3d(0.2, 1, 0.2);
    glVertex3d(0.2, 0, 0.2);

    glVertex3d(0.2, 0, 0);
    glVertex3d(0.2, 1, 0);
    glVertex3d(0.2, 1, 0.2);
    glVertex3d(0.2, 0, 0.2);

    glVertex3d(0, 1, 0);
    glVertex3d(0.2, 1, 0);
    glVertex3d(0.2, 1, 0.2);
    glVertex3d(0, 1, 0.2);

    glVertex3d(0, 0, 0);
    glVertex3d(0.2, 0, 0);
    glVertex3d(0.2, 0, 0.2);
    glVertex3d(0, 0, 0.2);

    glEnd();

    glPopMatrix();

    //jambe gauche
    glPushMatrix();
    glTranslatef(0.1, -1, 0.1);
    glRotatef(-angle, 0.5, 0, 0);

    glBegin(GL_QUADS);

    glVertex3d(0, 0, 0);
    glVertex3d(0, 1, 0);
    glVertex3d(0.2, 1, 0);
    glVertex3d(0.2, 0, 0);

    glVertex3d(0, 0, 0);
    glVertex3d(0, 1, 0);
    glVertex3d(0, 1, 0.2);
    glVertex3d(0, 0, 0.2);

    glVertex3d(0, 0, 0.2);
    glVertex3d(0, 1, 0.2);
    glVertex3d(0.2, 1, 0.2);
    glVertex3d(0.2, 0, 0.2);

    glVertex3d(0.2, 0, 0);
    glVertex3d(0.2, 1, 0);
    glVertex3d(0.2, 1, 0.2);
    glVertex3d(0.2, 0, 0.2);

    glVertex3d(0, 1, 0);
    glVertex3d(0.2, 1, 0);
    glVertex3d(0.2, 1, 0.2);
    glVertex3d(0, 1, 0.2);

    glVertex3d(0, 0, 0);
    glVertex3d(0.2, 0, 0);
    glVertex3d(0.2, 0, 0.2);
    glVertex3d(0, 0, 0.2);

    glEnd();

    glPopMatrix();


    glPopMatrix();
}
