#include "obstacle.h"
#include "construction.h"

double pos_x= 0, pos_y=2, pos_z=1; //Variable de position pour le perso
double eyex=0, eyey=4, eyez= 0, cx=0, cy=0, cz=0, upx=0, upy=1, upz=0; //Variable pour lookat
double t=0, h, saut,l; //Variable pour le saut
double sol = 0;
int angle=0, ROTATE=0; //variable pour animer le personnage

int z_obstacle = 10;//Generation en z des obstacles

int main(int argc, char *argv[]){
    GAME=0;
    FIN=0;
    VICTOIRE=0;
    sur_obstacle = 0;
    camion=0;
    
    glutInit(&argc, argv);

    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);

    glutInitWindowSize(1920, 1080);

    glutInitWindowPosition(50,50);

    glutCreateWindow("DodgeTown");

    glEnable(GL_DEPTH_TEST);
    glClearColor(0.4, 0.9, 0.9, 0.5);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glFrustum(-3.55554, 3.55555, -2, 2, 2, 500);
    
    z_obstacle = generer_obstacles(z_obstacle);
    glutDisplayFunc(Affichage);

    glutKeyboardFunc(Clavier);

    glutMouseFunc(Souris);
    
    glutIdleFunc(Animer);

    glutMainLoop();

    return 0;

}

//Gestion des touches du clavier
void Clavier(unsigned char key,int x,int y){

    switch (key){
    case('d'):  //se deplacer a droite
        if (pos_x > LIMITE_X_MIN)
            pos_x -= 0.1;
        glutPostRedisplay();
        break;

    case('q'):  //se deplacer a gauche
        if (pos_x + TAILLE_PERSO < LIMITE_X_MAX)
            pos_x += 0.1;
        glutPostRedisplay();
        break;        

    case (27):  //echap pour quitter
        exit(0);
        break;
        
    case(' '):  //se deplacer espace
        if(saut<2){
            h=pos_y;
            saut++;
            l = pos_z;
            t=0;
        }
        break;

    default :
        printf(" Touche: %c   Souris : %d %d \n", key, x, y);
        break;
        
    }
}

//Affiche le texte donnee a la position x y 
void texte(int x, int y, char* text){
    int i;
    glRasterPos2f(x,y);
    for(i=0; i<strlen(text); i++){
        glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24,text[i]);
    }
}

//Gestion des objets a afficher
void Affichage(){
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    if(GAME==0){    //premier menu de lancement
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        glOrtho(0, 1920, 0, 1080, 0, 1);

        glColor3ub(121, 90, 27);
        texte(930, 540, "Jouer");
        texte(925, 440, "Quitter");
        texte(50, 50, "Le but du jeu consiste a arriver jusqu'au tunnel pour fuir cette ville. Vous pouvez vous deplacer de gauche a droite avec les touches Q et D. Et vous pouvez sauter avec la touche espace. ");

        glColor3ub(255, 0, 15);
        texte(930, 740, "Menu");

        glColor3ub(255, 255, 0);
        glBegin(GL_QUADS);

        //cadre jouer
        glVertex2i(880, 510);
        glVertex2i(880, 580);
        glVertex2i(1040, 580);
        glVertex2i(1040, 510);

        //cadre quitter
        glVertex2i(880, 410);
        glVertex2i(880, 480);
        glVertex2i(1040, 480);
        glVertex2i(1040, 410);

        glEnd();

    }else if(FIN){  //Si defaite ou victoire
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        glOrtho(0, 1920, 0, 1080, 0, 1);

        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
    
        glColor3ub(255, 0, 15);
        if(VICTOIRE){
            texte(930, 740, "Victoire");
        }else{
            texte(930, 740, "Defaite");
        }

        glColor3ub(121, 90, 27);
        texte(925, 540, "Rejouer");
        texte(925, 440, "Quitter");

        glColor3ub(255, 255, 0);
        glBegin(GL_QUADS);

        //cadre jouer
        glVertex2i(880, 510);
        glVertex2i(880, 580);
        glVertex2i(1040, 580);
        glVertex2i(1040, 510);

        //cadre quitter
        glVertex2i(880, 410);
        glVertex2i(880, 480);
        glVertex2i(1040, 480);
        glVertex2i(1040, 410);

        glEnd();

    }else{  //jeu
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        glFrustum(-3.55554, 3.55555, -2, 2, 2, 500);

        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
        eyey = pos_y + 2;
        eyez = pos_z - 5;
        cz = pos_z;
        gluLookAt(eyex, eyey, eyez, cx, cy, cz, upx, upy, upz);

        
        affiche_personnage(pos_x, pos_y, pos_z, angle);
        affiche_decor(eyez);  
        afficher_obstacles(); 
        affiche_armature(LIMITE_X_MIN, 0,-1, LIMITE_X_MAX, 10, LIMITE_Z_MAX);
    }
    glutSwapBuffers();
}

//Gestion de l'animation
void Animer(){    
    if(GAME){//en jeu
        if(pos_z + TAILLE_PERSO < LIMITE_Z_MAX){
            pos_z += 0.5;
            if(saut>0){
                t+=.03;
                
                pos_y=-G*pow(t,2)+VITESSE*sin(ALPHA)*t+h;  //equation de trajectoire
                
                cy=pos_y-2;
                
                if(pos_y <= sol + 2){   
                    saut=0;
                    t=0;
                    
                    pos_y = sol + 2;
                    cy=pos_y-2;

                }
            }
            if(saut == 0 && sur_obstacle == 0 && pos_y > sol + 2){
                t+=.03;

                if(camion){
                    h=7.5;
                }else{
                    h=4.5;
                }
               // h=7.5;
                pos_y=-G*pow(t,2)+VITESSE*sin(0)*t+h;
                cy = pos_y - 2;
                if(pos_y <= sol + 2){   
                    saut=0;
                    t=0;
                    pos_y = sol + 2;
                    cy=pos_y-2;

                }
            }
            z_obstacle = test_obstacle(z_obstacle,pos_x,pos_y-2,pos_z,&sol);

            if(ROTATE){
                angle += 10;
                if (angle > 270){
                    ROTATE=0;
                }
            }else {
                angle -= 10;
                if (angle <90){
                    ROTATE=1;
                }
            } 
        }
        else{
            VICTOIRE=1;
            FIN=1;
        }
    }
    glutPostRedisplay();
}

//Gestion de la souris pour les menus
void Souris(int bouton, int etat, int x, int y){
	switch (bouton){
		case GLUT_LEFT_BUTTON :
			if (etat==GLUT_UP){

                if(GAME==0){
                    if(x<=1040 && x>=880 && y<=540 && y>=475){
                        GAME=1;
                    }

                    if(x<=1040 && x>=880 && y<=640 && y>=570){
                        exit(0);
                    }
                }

                if(FIN==1){
                    if(x<=1040 && x>=880 && y<=540 && y>=475){
                        z_obstacle = 10;
                        z_obstacle = generer_obstacles(z_obstacle);
                        VICTOIRE=0;
                        FIN=0;
                        pos_x=0;
                        pos_y=2;
                        pos_z=1;
                    }

                    if(x<=1040 && x>=880 && y<=640 && y>=570){
                        exit(0);
                    }
                }
            } 
			break;
		
		default :
			printf("Erreur souris\n");
			break;
	}
}
