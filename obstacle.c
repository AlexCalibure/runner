#include "obstacle.h"


coordonnes tab_obstacles[NB_OBSTACLE];
int num_obstacle;

/* Fonction qui génere un obstacle aleatoire au rang i dans tab_obstacle et le place a une distance z_obstacle*/
int genere_obstacle(int i, int z_obstacle){
    int type = rand()%5;/* Choix aleatoire du type */
    int couleur = rand()%3;/* Couleur aleatoire pour les voitures */
        if(type == TRONC){  
            tab_obstacles[i].x = LIMITE_X_MIN;
            tab_obstacles[i].y = 0;
            tab_obstacles[i].z = rand()%30 + z_obstacle;
            tab_obstacles[i].type = TRONC;
            tab_obstacles[i].couleur = 0;
            z_obstacle = tab_obstacles[i].z + 1;
        }
        else if (type == TROU){
            tab_obstacles[i].x = rand()%( (LIMITE_X_MAX-2)  - (LIMITE_X_MIN+2)) + (LIMITE_X_MIN + 2);
            tab_obstacles[i].y = 0;
            tab_obstacles[i].z = rand()%30 + z_obstacle;
            tab_obstacles[i].type = TROU;
            tab_obstacles[i].couleur = 0;
            z_obstacle = tab_obstacles[i].z + 4;
        }
        else if (type == PLOT){
            tab_obstacles[i].x = rand()%( (LIMITE_X_MAX-1)  - (LIMITE_X_MIN+1)) + (LIMITE_X_MIN + 1);
            tab_obstacles[i].y = 0;
            tab_obstacles[i].z = rand()%30 + z_obstacle;
            tab_obstacles[i].type = PLOT;
            tab_obstacles[i].couleur = 0;
            z_obstacle = tab_obstacles[i].z + 2;
        }
        else if (type == VOITURE){
            tab_obstacles[i].x = rand()%( (LIMITE_X_MAX)  - (LIMITE_X_MIN + 2)) + (LIMITE_X_MIN + 2);
            tab_obstacles[i].y = 0;
            tab_obstacles[i].z = rand()%30 + z_obstacle;
            tab_obstacles[i].type = VOITURE;
            tab_obstacles[i].couleur = couleur;
            z_obstacle = tab_obstacles[i].z + 10;
        }
        else if (type == CAMION){
            tab_obstacles[i].x = rand()%( (LIMITE_X_MAX)  - (LIMITE_X_MIN + 3) ) + (LIMITE_X_MIN + 3);
            tab_obstacles[i].y = 0;
            tab_obstacles[i].z = rand()%30 + z_obstacle;
            tab_obstacles[i].type = CAMION;
            tab_obstacles[i].couleur = couleur;
            z_obstacle = tab_obstacles[i].z + 10;
        }
    return z_obstacle;
}
/* Fonction qui initialise tab_obstacle */
int generer_obstacles(int z_obstacle){
    int i;
    num_obstacle = 0;
    srand(time(NULL));
    for(i = 0; i < NB_OBSTACLE; i++){
        z_obstacle = genere_obstacle(i,z_obstacle);
    }
    return z_obstacle;
}
/* Fonction d'affichage des obsatcles */
void afficher_obstacles(){
    int i;
    srand(time(NULL));
    for(i = 0; i < NB_OBSTACLE; i++){
        if (tab_obstacles[i].type == TRONC){
            affiche_tronc(tab_obstacles[i].z);
        }
        else if (tab_obstacles[i].type == TROU){
            affiche_trou(tab_obstacles[i].x,tab_obstacles[i].y,tab_obstacles[i].z);
        }
        else if (tab_obstacles[i].type == PLOT){
            affiche_plot(tab_obstacles[i].x,tab_obstacles[i].y,tab_obstacles[i].z);
        }
        else if (tab_obstacles[i].type == VOITURE){
            /* Test pour l'orientation de la voiture */
            if (tab_obstacles[i].x <= 0)
                affiche_voiture(tab_obstacles[i].x,tab_obstacles[i].y,tab_obstacles[i].z,0,tab_obstacles[i].couleur);
            else{
                affiche_voiture(tab_obstacles[i].x,tab_obstacles[i].y,tab_obstacles[i].z,1,tab_obstacles[i].couleur);
            }
        }
        else if (tab_obstacles[i].type == CAMION){
            /* Test pour l'orientation du camion */
            if (tab_obstacles[i].x - 2 <= 0){
                affiche_camion(tab_obstacles[i].x,tab_obstacles[i].y,tab_obstacles[i].z,0);
            }
            else{
                affiche_camion(tab_obstacles[i].x,tab_obstacles[i].y,tab_obstacles[i].z,1);
            }
        }
    }
}
/* Fonction de test de colision avec les obstacles */
int test_obstacle(int z_obstacle, double pos_x, double pos_y, double pos_z, double *sol){
    int i;
    
    /* Remplace les obstacles qui ne sont plus affichés */
    if( pos_z - 15 > tab_obstacles[num_obstacle].z){
        if (z_obstacle + 30 < LIMITE_Z_MAX){
            z_obstacle = genere_obstacle(num_obstacle,z_obstacle);
        }
        num_obstacle = (num_obstacle+1)%NB_OBSTACLE;
    }

    else{
        *sol = 0;
        sur_obstacle = 0;
        /* Test la colision en prenant un point au centre du personnage avec les 5 obstacles les plus proches*/
        for(i = 0; i < 5; i++){
            if (tab_obstacles[num_obstacle + i].type == TRONC){
                if( pos_z + TAILLE_PERSO/2 >= tab_obstacles[num_obstacle + i].z - 1  && pos_z + TAILLE_PERSO/2 <= tab_obstacles[num_obstacle + i ].z + 1 &&  pos_y < tab_obstacles[num_obstacle + i].y + 1){
                    FIN = 1;
                }
            }
            if (tab_obstacles[num_obstacle + i].type == TROU){
                if(pos_z + TAILLE_PERSO/2 >= tab_obstacles[num_obstacle + i].z - 2 && pos_z + TAILLE_PERSO/2 <= tab_obstacles[num_obstacle + i].z + 2 && pos_x + TAILLE_PERSO/2 >= tab_obstacles[num_obstacle + i].x - 2 && pos_x + TAILLE_PERSO/2 <= tab_obstacles[num_obstacle + i].x + 2 && pos_y == tab_obstacles[num_obstacle + i].y){
                    FIN = 1;
                }
            }
            if (tab_obstacles[num_obstacle + i].type == PLOT){
                if(pos_z + TAILLE_PERSO/2 >= tab_obstacles[num_obstacle + i].z - 1  && pos_z + TAILLE_PERSO/2 <= tab_obstacles[num_obstacle + i].z + 1 && pos_x + TAILLE_PERSO/2 >= tab_obstacles[num_obstacle + i].x - 0.8 && pos_x + TAILLE_PERSO/2 <= tab_obstacles[num_obstacle + i].x + 0.3 && pos_y >= tab_obstacles[num_obstacle + i].y && pos_y <= tab_obstacles[num_obstacle + i].y + 1.2){
                    FIN = 1;
                } 
            }
            if (tab_obstacles[num_obstacle + i].type == VOITURE){
                if(pos_z + TAILLE_PERSO/2 >= tab_obstacles[num_obstacle + i].z && pos_z + TAILLE_PERSO/2 <= tab_obstacles[num_obstacle + i].z + 5 && pos_x + TAILLE_PERSO/2 >= tab_obstacles[num_obstacle + i].x - 2.3 && pos_x + TAILLE_PERSO/2 <= tab_obstacles[num_obstacle + i].x + 0.3){
                    if (pos_y < 2.5 ) {
                        FIN = 1; 
                    }
                    else
                    {
                    *sol = 2.5;/* Le sol devient le toit de la voiture */
                    sur_obstacle = 1;
                    camion=0;
                    }
                
                } 
            }
            if (tab_obstacles[num_obstacle + i].type == CAMION){
                if(pos_z + TAILLE_PERSO/2 >= tab_obstacles[num_obstacle + i].z && pos_z + TAILLE_PERSO/2 <= tab_obstacles[num_obstacle + i].z + 10 && pos_x + TAILLE_PERSO/2 >= tab_obstacles[num_obstacle + i].x - 4.3 && pos_x + TAILLE_PERSO/2 <= tab_obstacles[num_obstacle + i].x + 0.3 ){
                    if (pos_y < 5 ){
                        FIN = 1;
                    }
                    else
                    {
                        *sol = 5.5;/* Le sol devient le toit du camion */
                        sur_obstacle = 1;
                        camion=1;
                    }
                
                } 
            }
        }
    }
    return z_obstacle;
}