#include <stdio.h>
#include <stdlib.h>
#include "GL/gl.h"
#include "GL/glut.h"
#include <unistd.h>

void affiche_cube(double x1, double y1, double z1, double x2, double y2, double z2);

void affiche_sphere(double x, double y, double z, double rayon, double r, double v, double b);

void affiche_armature(double x1, double y1, double z1, double x2, double y2, double z2);

void affiche_maison(double x1, double y1, double z1, int cote, int offset);

void affiche_arbre(double x, double y, double z);

void affiche_poteau(double offset);

void affiche_jardin();

void affiche_haie();

void affiche_montagne();

void affiche_decor(double z);

void affiche_voiture(double x, double y, double z, int angle, int couleur);

void affiche_plot(double x, double y, double z);

void affiche_trou(double x, double y, double z);

void affiche_tronc(double z );

void affiche_camion(double x, double y, double z, int angle);

void affiche_personnage(double x, double y, double z, double angle);