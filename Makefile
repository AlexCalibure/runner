CC = gcc
OPTIONS = -Wall
OptGL= -lglut -lGLU -lGL -lm

all : jeu

jeu : construction.o obstacle.o
	$(CC) $(OPTIONS) jeu.c construction.o obstacle.o -o jeu $(OptGL)

construction.o: construction.c construction.h 
	$(CC) $(OPTIONS) -c construction.c -o construction.o $(OptGL)

obstacle.o: obstacle.c obstacle.h 
	$(CC) $(OPTIONS) -c obstacle.c -o obstacle.o $(OptGL)

clean :
	rm -f *.o jeu