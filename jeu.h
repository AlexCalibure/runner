#ifndef _JEU_H_
#define _JEU_H_

#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include "GL/gl.h"
#include "GL/glut.h"

#define LIMITE_X_MIN -5
#define LIMITE_X_MAX 5
#define LIMITE_Z_MAX 500

#define TAILLE_PERSO 1

#define PI 3.14
#define G 19.81
#define ALPHA PI/4
#define VITESSE 22

int GAME, FIN, VICTOIRE;
int sur_obstacle;
int camion; //savoir si l'obsatcel est un camion ou une voiture

void Clavier(unsigned char key,int x,int y);
void texte(int x, int y, char* text);
void Affichage();
void Animer();
void Souris(int bouton, int etat, int x, int y);

#endif
